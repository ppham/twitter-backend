# Config Exercise

The purpose of this exercise is to read a config file of key/value pairs and produce a data structure that can be queried at runtime.
To run this, you'll need the following:

* Node v11.14.0 or later
* Yarn v1.22.4 or later
* Typescript 

## How To Run

```
git clone https://gitlab.com/ppham/twitter-backend
cd twitter-backend
yarn
yarn build
node loadConfig.js
```

## Complexity Analysis

If the length of the config file is `N` lines, the algorithm parses the config file in `O(N)` time, reading
once per line, and adding to a hash map which ultimately store `O(N)` keys/values.

Querying the config map, once loaded, is `O(1)`, with an initial `O(N)` time to apply override filters,
which can be amortized across all subsequent queries.

If there are `q` possible overrides, then there are `Q=2^q` possible override combinations, meaning at most,
`O(QN)` space and time is needed to store `Q` config JSON objects. In reality, this space requirement is much
less, as most overrides share their non-overridden keys and values. However, our current implementation is too
simple to do this optimization.

## Design

### Libraries and Language
I used Typescript for typechecking, auto-complete, and linting. In VSCode, it was extremely easy to
rapidly prototype a choice of data structures and stub out an implementation sketch using method signatures.

I also make extensive use of Facebook's ImmutableJS library for functional-style immutable collection handling.
This gives us the option of keeping a history of config changes for debugging.

### Features
Given the config file format below, our job is to write the parser which will create a data structure in-memory to support the following features:

* Grouped key-value pairs (first level of hierarchy) where the name of each group is written in brackets.
* A second-level of key-value pairs after each group heading belong to that group, and are not nested (the second level of hierarchy).
* Optional overrides for each key (a third level of hierarchy)
* Comments (lines beginning with semi-colons) and other whitespaces outside of quotes should be ignored.
* A JSON object is expected to be produced from the config file, which can support large numbers of queries.
* Values can be of type string, but should be converted as specifically as possible to also support
  * integers
  * booleans with truthy and falsey values
  * an array of comma-separated strings.

```
[common]
basic_size_limit = 26214400
student_size_limit = 52428800
paid_users_size_limit = 2147483648
path = /srv/var/tmp/
path<itscript> = /srv/tmp/
[ftp]
name = "hello there, ftp uploading"
path = /tmp/
path<production> = /srv/var/tmp/
path<staging> = /srv/uploads/
path<ubuntu> = /etc/var/uploads
enabled = no
; This is a comment
[http]
name = "http uploading"
path = /tmp/
path<production> = /srv/var/tmp/
path<staging> = /srv/uploads/; This is another comment
params = array,of,values
```

## Data structure

We created a `ConfigMap` data structure with different Typescript types for each level of the three-level hierarchy:
```
type ValueType = string | boolean | number | Array<string>

// Map of group name to a sub-map, of key to another sub-map (ordered), of override name to value
type GroupedKeyValueMap = Map<String,KeyValueMap>
type KeyValueMap = Map<String,OverrideKeyValueMap>
type OverrideKeyValueMap = OrderedMap<String,ValueType>

class ConfigMap {
    readonly groupedKeyValues: GroupedKeyValueMap
    readonly errors: List<String>
    readonly currentGroup: String
    readonly parent: ConfigMap | null

    constructor(...) { ... }

    static newEmpty() { ... }

    mergeGroupName(groupName: String): void { ... }

    mergeError(errorString: String): void { ... }

    mergeKeyValue(key: string, value: string, override: string = ''):void { ... }

    toJSON(_overrideFilters: List<String>=List([])): {[key: string]: any} { ... }
}
```

The data structure includes a list of errors which indicate parsing failures on specific lines,
but our parser is designed to continue and do the best job it can, since syntax errors on one line
are usually independent of other lines.

The same ConfigMap can produce different config JSON objects via `toJSON()` method by passing
in a list of different override filters. The default filter of `""` is always added, and we used
an `OrderedMap` when parsing the overrides so that the last override in a file appears last
in the map. When we filter for override values, we use `List.last()` method to choose the last
defined override of all the matching overrides.

## Parser

We separate out the parsing of config file and building of `ConfigMap` into its own function,
which uses `List.reduce`:

```
function loadConfig(filename: string, overrides: Array<String>) {
    const lines: List<String> = List(readFileSync(filename).toString().split('\n'))

    return lines.reduce((
            sum: ConfigMap,
            line: String,
            i: Number,
            iter: List<String>
        ): ConfigMap => {

        // Try matching the line as a group name, if so return early with sum.mergeGroupName

        // Strip any comments at end of line. If it's the whole line, return early without changes.

        // If it's not a group name, parse it like a key value pair by splitting on `=` and trim spaces

        // If there are any errors, return early with sum.mergeError

        // Match any overrides with <>

        // Add the key and value, with any overrides, with sum.mergeKeyValue
        return sum.mergeKeyValue(key, value, override)

    }, initialEmptyConfig)
}

```

## Alternatives

I finished most of the implementation designing for just two-levels of hierarchy, before realizing
that supporting overrides was a more complicated feature than just a third level of hierarchy.

I was tempted to rewrite the `ConfigMap` to support all three levels of the hierarchy uniformly,
but ran out of time. As a result, `ConfigMap.mergeKeyValue` is more complicated than I would like,
to handle populating the three different levels of config.

An alternative to a single `ConfigMap` supporting multiple overrides by producing
multiple deep-copied JSON objects is to filter out the desired overrides on every query.
For example, the file can be read once for each new set of overrides, and then freed to make
room for the next set of overrides. But this is unnecessarily slow
and may only desirable if we have millions of override values, such that storing them all in memory
is infeasible.

## Improvements

Some configs benefit from arbitrarily nested keys such as `ftp.url.host`. To support deeply nested
key-values we would probably get rid of groups and use fully-qualified, dot-separated nested key names,
and then have a generic `NestedKeyValueMap` class that recursively passes `mergeKeyValue` requests
to its sub-config children, using a `List` of `keyTuples` that gets popped at every level.