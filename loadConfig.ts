import { readFileSync } from 'fs'
import { List, Map, OrderedMap } from 'immutable'
import { assert } from 'chai'

function toJS(inputMap: Map<any,any>): {[key: string]: any} {
    let result = {}
    inputMap.map((value, key) => {
        const finalValue = (Map.isMap(value) || OrderedMap.isOrderedMap(value)) ? toJS(value) : value
        result[key] = finalValue
    })
    return result
}

function parseValue(value: string): ValueType {
    // Try if it's an integer
    {
        const intValue = parseInt(value)
        if (!isNaN(intValue)) {
            return intValue
        }    
    }

    // If it's quoted, just return the value
    if (value.startsWith('"') && value.endsWith('"')) {
        return value.substr(1, value.length-2)
    }

    {
        const isFalsey = (value === 'no' || value === 'false' || value === '0')
        const isTruthy = (value === 'yes' || value === 'true' || value === '1')
        if (isFalsey) {
            return false
        } else if (isTruthy) {
            return true
        }
    }

    // Try if it's a list
    {
        const tokens = value.split(',')
        if (tokens.length > 1) {
            return tokens
        }
    }

    // Last resort, it is a string
    return value
}

type ValueType = string | boolean | number | Array<string>

// Map of group name to a sub-map, of key to another sub-map (ordered), of override name to value
type GroupedKeyValueMap = Map<String,KeyValueMap>
type KeyValueMap = Map<String,OverrideKeyValueMap>
type OverrideKeyValueMap = OrderedMap<String,ValueType>

class ConfigMap {
    readonly groupedKeyValues: GroupedKeyValueMap
    readonly errors: List<String>
    readonly currentGroup: String
    readonly parent: ConfigMap | null

    constructor(
        _parent: ConfigMap | null = null,
        _groupedKeyValues: GroupedKeyValueMap,
        _errors: List<String>,
        _currentGroup: String = '', 
        ) {
        console.log(`Previous config map`, JSON.stringify(_parent && _parent.toJSON(), null, 2))
        assert(_groupedKeyValues)
        console.log(`Previous groupedKeyValues`, JSON.stringify(_groupedKeyValues && toJS(_groupedKeyValues), null, 2))
        this.groupedKeyValues = _groupedKeyValues || Map({})
        this.errors = _errors || List()
        this.currentGroup = _currentGroup
        this.parent = _parent
    }

    static newEmpty() {
        return new ConfigMap(null, Map({}), List(), '')
    }

    mergeGroupName(groupName: String) {
        return new ConfigMap(this, this.groupedKeyValues, this.errors, groupName);
    }

    mergeError(errorString: String) {
        return new ConfigMap(this, this.groupedKeyValues, this.errors.push(errorString), this.currentGroup);
    }

    mergeKeyValue(key: string, value: string, override: string = '') {
        console.log(`Merging ${key} ${value} for group name ${this.currentGroup}`)
        const mapWithKey: KeyValueMap = this.groupedKeyValues.get(this.currentGroup) || Map({})
        const mapWithOverride: OverrideKeyValueMap = mapWithKey.get(key) || OrderedMap({})

        const parsedValue = parseValue(value)

        const newOverrideGroupKeyValues = mapWithKey.set(key, mapWithOverride.set(override, parsedValue))
        const newGroupedKeyValues: GroupedKeyValueMap = this.groupedKeyValues.set(this.currentGroup, newOverrideGroupKeyValues)
        console.log(`New grouped key values ${JSON.stringify(newGroupedKeyValues.toJS())}`)
        return new ConfigMap(this, newGroupedKeyValues, this.errors, this.currentGroup)
    }

    toJSON(_overrideFilters: List<String>=List([])): {[key: string]: any} {
        // we always add the default override (no override)
        const overrideFilters = _overrideFilters.push("")
        return toJS(this.groupedKeyValues.map((mapWithKey: KeyValueMap, key: String) => {
            return mapWithKey.map((overrideGroupKeyValue: OverrideKeyValueMap) => {
                return overrideGroupKeyValue.filter((value: ValueType, override: string) => {
                    return overrideFilters.find((val) => (val === override)) !== undefined
                }).last()
            })
        }))
    }
}

function loadConfig(filename: string, overrides: Array<String>) {
    const lines: List<String> = List(readFileSync(filename).toString().split('\n'))

    const initialConfigMap: ConfigMap = ConfigMap.newEmpty()

    return lines.reduce((
            sum: ConfigMap,
            line: String,
            i: Number,
            iter: List<String>
        ): ConfigMap => {

        // Match group name as string of at least one alphanumeric word character between square brackets
        const groupNameMatches = line.match(/\[(\w+)\]/)
        if (groupNameMatches && groupNameMatches.length >= 2) {
            const groupName = groupNameMatches[1]
            if (groupName.length === 0) {
                return sum.mergeError(`Line ${i} has group name of zero length: ${line}`)
            } else {
                return sum.mergeGroupName(groupNameMatches[1])
            }
        }

        // Strip any comments at end of line
        const commentTokens = line.split(';')
        const strippedLine = (commentTokens.length > 1) ? commentTokens[0] : line

        if (strippedLine === '') {
            // the whole line is a comment, just return the previous config map
            return sum
        }

        // If it's not a group name, parse it like a key value pair
        const tokens = strippedLine.split('=')
        if (tokens.length < 2) {
            return sum.mergeError(`Line ${i} did not have tokens separated by =: \n${line}`)
        }

        const rawKey = tokens[0].trim()
        const value = tokens[1].trim()
        const overrideMatches = rawKey.match(/(\w+)<(\w+)\>/)
        const hasOverride = (overrideMatches && overrideMatches.length >= 3)
        const override = hasOverride ? overrideMatches[2] : ''
        const key = hasOverride ? overrideMatches[1] : rawKey
        return sum.mergeKeyValue(key, value, override)

    }, initialConfigMap)
}

function test(overrides?: Array<String>) {
    const configMap = loadConfig('config.txt', overrides)

    // Print any errors, then proceed with the config map that we have
    if (!configMap.errors.isEmpty()) {
        console.error("Config file parsing errors", ...configMap.errors.toJSON())
    }

    return configMap.toJSON(List(overrides))
}

// Run tests
{
    const CONFIG = test()

    assert.equal(CONFIG.ftp.name, 'hello there, ftp uploading')
    assert.equal(JSON.stringify(CONFIG.http.params), '["array","of","values"]')
    assert.equal(CONFIG.ftp.path, '/tmp/')
    assert.equal(CONFIG.common.paid_users_size_limit, '2147483648')
    assert.equal(JSON.stringify(CONFIG.ftp), '{"name":"hello there, ftp uploading","path":"/tmp/","enabled":false}')    
}

{
    const CONFIG = test(["ubuntu", "production"])

    console.log(JSON.stringify(CONFIG))
    
    assert.equal(CONFIG.ftp.name, 'hello there, ftp uploading')
    assert.equal(JSON.stringify(CONFIG.http.params), '["array","of","values"]')
    assert.equal(CONFIG.ftp.path, '/etc/var/uploads')
    assert.equal(CONFIG.common.paid_users_size_limit, '2147483648')
    assert.equal(CONFIG.ftp.enabled, false)
    assert.equal(JSON.stringify(CONFIG.ftp), '{"name":"hello there, ftp uploading","path":"/etc/var/uploads","enabled":false}')
}
